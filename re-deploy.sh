#!/bin/bash

current_version=`cat version.txt`
echo "Deploying the watches-v1 service with version: $current_version"
kubectl set image deployment watches-v1 watches-v1=harperkej/watches-v1:"$current_version"
