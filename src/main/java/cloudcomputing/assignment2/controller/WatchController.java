package cloudcomputing.assignment2.controller;

import cloudcomputing.assignment2.dto.WatchDto;
import cloudcomputing.assignment2.entity.WatchEntity;
import cloudcomputing.assignment2.exception.ApplicationException;
import cloudcomputing.assignment2.service.WatchService;
import cloudcomputing.assignment2.util.Mapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.util.MultiValueMap;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("watches/v1/watch")
public class WatchController {

    @Autowired
    private Mapper mapper;

    @Autowired
    private WatchService watchService;

    @RequestMapping(method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    public WatchDto createWatch(@RequestBody WatchDto watchDto) throws ApplicationException {
        WatchEntity watchEntity = this.mapper.toEntity(watchDto);
        watchEntity = this.watchService.create(watchEntity);
        return this.mapper.toDto(watchEntity);
    }

    @RequestMapping(value = "{sku}", method = RequestMethod.GET, produces = MediaType.APPLICATION_XML_VALUE)
    public WatchDto findWatchBySku(@PathVariable(value = "sku", required = true) String sku) throws ApplicationException {
        WatchEntity watchEntity = this.watchService.findBySku(sku);
        return this.mapper.toDto(watchEntity);
    }

    @RequestMapping(value = "{sku}", method = RequestMethod.POST, consumes = MediaType.APPLICATION_FORM_URLENCODED_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    public WatchDto updateWatchBySKu(@PathVariable(value = "sku", required = true) String sku, WatchDto watch) throws ApplicationException {
        WatchEntity updatedEntity = this.watchService.updateBySku(sku, watch);
        return this.mapper.toDto(updatedEntity);
    }

    @RequestMapping(value = "{sku}", method = RequestMethod.DELETE, produces = MediaType.APPLICATION_JSON_VALUE)
    public void delete(@PathVariable(value = "sku", required = true) String sku) throws ApplicationException {
        this.watchService.delete(sku);
    }

    @RequestMapping(value = "find/{sku}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public List<WatchDto> search(@PathVariable(value = "sku", required = true) String sku, @RequestParam(value = "type", required = false) String type,
                                 @RequestParam(value = "status", required = false) String status,
                                 @RequestParam(value = "gender", required = false) String gender,
                                 @RequestParam(value = "year", required = false) Integer year) throws ApplicationException {
        List<WatchEntity> watchEntities = this.watchService.search(sku, type, status, gender, year);
        return this.mapper.toDtoList(watchEntities);
    }

    @RequestMapping(method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public List<WatchDto> findAllWatches() {
        List<WatchEntity> watchEntities = this.watchService.findAll();
        return this.mapper.toDtoList(watchEntities);
    }

}
