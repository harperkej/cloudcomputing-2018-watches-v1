package cloudcomputing.assignment2.controller;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;


@RestController
@RequestMapping("/")
public class HealthCheck {

    @RequestMapping(value = "/", method = RequestMethod.GET)
    public ResponseEntity check() {
        return new ResponseEntity(HttpStatus.OK);
    }

}
