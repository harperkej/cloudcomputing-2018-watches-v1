package cloudcomputing.assignment2.controller;

import cloudcomputing.assignment2.dto.ExceptionResponseDto;
import cloudcomputing.assignment2.exception.ApplicationException;
import cloudcomputing.assignment2.exception.ExceptionReason;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;

import java.time.LocalDateTime;

@ControllerAdvice
public class GLobalExceptionHandler {

    @ResponseBody
    @ExceptionHandler(ApplicationException.class)
    public ResponseEntity<ExceptionResponseDto> handleGLobalExceptions(ApplicationException appException) {

        ExceptionResponseDto responseDto = new ExceptionResponseDto();
        responseDto.setTimestamp(LocalDateTime.now().toString());

        if (appException.getCasue() == ExceptionReason.DUPLICATE_KEY) {
            responseDto.setMessage(appException.getCasue().getReason() + " SKU = " + appException.getMessage());
            return buildResponse(responseDto, HttpStatus.BAD_REQUEST);
        } else if (appException.getCasue() == ExceptionReason.ENUM_EXCEPTION) {
            responseDto.setMessage(appException.getCasue().getReason() + " " + appException.getMessage());
            return buildResponse(responseDto, HttpStatus.BAD_REQUEST);
        } else if (appException.getCasue() == ExceptionReason.RESOURCE_NOT_FOUND) {
            responseDto.setMessage(appException.getCasue().getReason() + " " + appException.getMessage());
            return buildResponse(responseDto, HttpStatus.NOT_FOUND);
        } else if (appException.getCasue() == ExceptionReason.MISSING_FIELD) {
            responseDto.setMessage(appException.getCasue().getReason() + " " + appException.getMessage() );
            return buildResponse(responseDto, HttpStatus.BAD_REQUEST);
        } else {
            responseDto.setMessage("Something went wrong on the server. Please try again.");
            return buildResponse(responseDto, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    private ResponseEntity<ExceptionResponseDto> buildResponse(ExceptionResponseDto dto, HttpStatus status) {
        return new ResponseEntity<ExceptionResponseDto>(dto, status);
    }

}
