package cloudcomputing.assignment2.dto;

import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlRootElement;

@JacksonXmlRootElement(localName = "Watch")
public class WatchDto {

    private String sku;

    private String bracelet_material;

    private String case_form;

    private String case_material;

    private String dial_color;

    private String dial_material;

    private String movement;

    private String status;

    private String type;

    private String gender;

    private Integer year;

    public String getBracelet_material() {
        return bracelet_material;
    }

    public void setBracelet_material(String bracelet_material) {
        this.bracelet_material = bracelet_material;
    }

    public String getCase_form() {
        return case_form;
    }

    public void setCase_form(String case_form) {
        this.case_form = case_form;
    }

    public String getCase_material() {
        return case_material;
    }

    public void setCase_material(String case_material) {
        this.case_material = case_material;
    }

    public String getDial_color() {
        return dial_color;
    }

    public void setDial_color(String dial_color) {
        this.dial_color = dial_color;
    }

    public String getDial_material() {
        return dial_material;
    }

    public void setDial_material(String dial_material) {
        this.dial_material = dial_material;
    }

    public String getMovement() {
        return movement;
    }

    public void setMovement(String movement) {
        this.movement = movement;
    }

    public String getSku() {
        return sku;
    }

    public void setSku(String sku) {
        this.sku = sku;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public Integer getYear() {
        return year;
    }

    public void setYear(Integer year) {
        this.year = year;
    }
}
