#!/bin/bash

current_version=`cat version.txt`

echo '********************** Login to docker hub registry **********************'
docker login --username=$DOCKER_HUB_USERNAME --password=$DOCKER_HUB_PASSWORD
echo '********************** Build the docker image **********************'
docker build -t harperkej/watches-v1:"$current_version" docker/spring-boot-app/.
echo '********************** Push the docker image to repository **********************'
docker push harperkej/watches-v1:"$current_version"
rm docker/spring-boot-app/assignment2-0.0.1-SNAPSHOT.jar
