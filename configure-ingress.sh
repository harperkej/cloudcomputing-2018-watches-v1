#!/bin/bash

echo 'Configuring the ingress controller to route request to watches-v1 deployment'
kubectl apply -f ingress.yaml
