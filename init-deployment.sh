#!/bin/bash

echo 'Initial deployment of the service watches-v1'
kubectl create -f watches-v1-deployment.yaml

echo 'Exposing the watches-v1 deployment as a service internally.'
kubectl expose deployment watches-v1 --target-port=80 --type=NodePort

